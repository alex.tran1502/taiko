use clap::{App, AppSettings, Arg, SubCommand};

#[derive(Debug)]
pub struct TaikoRunCommand {
	pub weather_app: WeatherAppCommand,
	pub todo_app: TodoAppCommand
}
	
#[derive(Debug)]
pub struct WeatherAppCommand {
    pub get_weather_now: bool,
    pub get_weather_tomorrow: bool,
    pub set_weather_apikey: bool,
    pub weather_apikey: Option<String>,
    pub zipcode: u32,
}

#[derive(Debug)]
pub struct TodoAppCommand {
    pub postgresql_connection_string: Option<String>,
}

impl Default for TaikoRunCommand {
    fn default() -> Self {
        let weather_app_cmd = WeatherAppCommand {
            get_weather_now: false,
            get_weather_tomorrow: false,
            set_weather_apikey: false,
            weather_apikey: None,
            zipcode: 0,
        };

		let todo_app_cmd = TodoAppCommand {
			postgresql_connection_string: None
		};

        TaikoRunCommand {
			weather_app: weather_app_cmd,
			todo_app: todo_app_cmd,
        }
    }
}
pub fn get_command() -> Result<TaikoRunCommand, Box<dyn std::error::Error>> {
    let mut taiko_run_cmd: TaikoRunCommand = Default::default();

    let matches = App::new(clap::crate_name!())
        .setting(AppSettings::DontDelimitTrailingValues)
        .setting(AppSettings::ColoredHelp)
        .setting(AppSettings::ArgRequiredElseHelp)
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .subcommand(
            SubCommand::with_name("weather")
                .setting(AppSettings::ArgRequiredElseHelp)
                .about("Getting Weather Information")
                .subcommand(SubCommand::with_name("now").about("Get Current Weather"))
                .subcommand(SubCommand::with_name("tomorrow").about("Get Tomorrow Weather"))
                .subcommand(
                    SubCommand::with_name("setting")
                        .setting(AppSettings::ArgRequiredElseHelp)
                        .about("Set API Key")
                        .arg(
                            Arg::with_name("key")
                                .long("key")
                                .value_name("API KEY")
                                .required(true)
                                .help("openweathermap api key"),
                        )
                        .arg(
                            Arg::with_name("zipcode")
                                .long("zipcode")
                                .value_name("ZIPCODE")
                                .required(true)
                                .help("Zip Code"),
                        ),
                ),
        )	
        .subcommand(
            SubCommand::with_name("todo")
                .setting(AppSettings::ArgRequiredElseHelp)
                .about("A command line todo app helper")
                .subcommand(
                    SubCommand::with_name("setting")
                        .setting(AppSettings::ArgRequiredElseHelp)
                        .about("Set API Key")
                        .arg(
                            Arg::with_name("conn_str")
								.long("connection_string")
								.short("c")
                                .value_name("POSTGRESQL CONNECTION STRING")
                                .required(true)
                                .help("PostgreSQL Connection String"),
                        ),
                )
                .subcommand(
                    SubCommand::with_name("add")
                        .setting(AppSettings::ArgRequiredElseHelp)
                        .about("Adding a todo item in the list with an optional deadline")
                        .arg(
                            Arg::with_name("title")
                                .long("title")
                                .short("t")
                                .value_name("TITLE")
                                .help("Task title")
                                .required(true),
                        )
                        .arg(
                            Arg::with_name("content")
                                .long("content")
                                .short("c")
                                .value_name("CONTENT")
                                .help("Task Description")
                                .required(true),
                        )
                        .arg(
                            Arg::with_name("duedate")
                                .long("due")
                                .short("d")
                                .value_name("DUE DATE")
                                .help("Due Date - Will remind you over email"),
                        ),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("weather", Some(weather_match)) => match weather_match.subcommand() {
            ("now", Some(_)) => taiko_run_cmd.weather_app.get_weather_now = true,
            ("tomorrow", Some(_)) => taiko_run_cmd.weather_app.get_weather_tomorrow = true,
            ("setting", Some(setkey_match)) => {
                taiko_run_cmd.weather_app.set_weather_apikey = true;
                taiko_run_cmd.weather_app.weather_apikey =
                    setkey_match.value_of("key").map(String::from);
                taiko_run_cmd.weather_app.zipcode =
                    setkey_match.value_of("zipcode").unwrap().parse().unwrap();
            }
            _ => {}
        },
        ("todo", Some(todo_match)) => match todo_match.subcommand() {
            ("add", Some(add_match)) => {
                println!("Match todo key {:?}", add_match);
			},
			("setting", Some(set_conn_str_match)) => {
				taiko_run_cmd.todo_app.postgresql_connection_string = set_conn_str_match.value_of("conn_str").map(String::from);
			},
            _ => {}
        },
        _ => {}
    }

	println!("CMD Trace: {:#?}", taiko_run_cmd);
    Ok(taiko_run_cmd)
}
