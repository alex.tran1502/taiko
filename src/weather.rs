use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs, path::PathBuf}; 
use dirs;

#[derive(Debug, Serialize, Deserialize)]
pub struct WeatherSetting {
    pub open_weather_map_api_key: String,
    pub zipcode: u32,
}

#[allow(dead_code)]
impl WeatherSetting {
    fn get_setting_file_location() -> std::path::PathBuf {
        let home_dir: PathBuf = match dirs::home_dir() {
            Some(path) => {
                let mut file_location: PathBuf = PathBuf::new();
                file_location.push(path);
                file_location.push(".weather_setting.json");
                file_location
            },
            None => panic!("No Home Path Found"),
        };

        home_dir
    }

    pub fn set(apikey: &str, zipcode: u32) -> Result<&str, Box<dyn std::error::Error>> {
        let home_dir = Self::get_setting_file_location();

        println!("Setting File is saved to {:?}", home_dir);

        let weather_setting = WeatherSetting {
            open_weather_map_api_key: apikey.to_string(),
            zipcode,
        };

        let setting_file_fd = &fs::File::create(home_dir).unwrap();

        serde_json::to_writer_pretty(setting_file_fd, &weather_setting).unwrap();

        Ok("Set Open Weather API Key Success")
    }

    fn read() -> Self {
        let home_dir = Self::get_setting_file_location();

        let open_setting_file = match fs::File::open(home_dir) {
            Ok(fd) => fd,
            Err(e) => {
                panic!("Read File Error: {:#?}", e);
            }
        };

        let read_result: Self = match serde_json::from_reader(open_setting_file) {
            Ok(v) => v,
            Err(e) => panic!("Parse Setting File Error: {:#?}", e),
        };

        read_result
    }
}

#[derive(Debug, Deserialize)]
pub struct CurrentWeather {
    pub coord: HashMap<String, f64>,
    pub weather: Vec<WeatherWeather>,
    pub main: CurrentWeatherMain,
}

#[derive(Debug, Deserialize)]
pub struct WeatherWeather {
    pub icon: String,
    pub description: String,
    pub id: i32,
    pub main: String,
}

#[derive(Debug, Deserialize)]
pub struct CurrentWeatherMain {
    pub temp: f64,
    pub temp_max: f64,
    pub temp_min: f64,
    pub feels_like: f64,
    pub pressure: f64,
    pub humidity: f64,
}

impl CurrentWeather {
    pub async fn print() -> Result<(), Box<dyn std::error::Error>> {
        let api_value: WeatherSetting = WeatherSetting::read();

        let request_url = format!(
        "https://api.openweathermap.org/data/2.5/weather?zip={zipcode},us&appid={api_key}&units=imperial",
        zipcode = api_value.zipcode,
        api_key = api_value.open_weather_map_api_key
    );

        let response = reqwest::get(&request_url).await?;

        let current_weather: Self = match response.status() {
            StatusCode::OK => serde_json::from_str(&response.text().await?)?,
            status => panic!("Request Failed With Code {:?}", status),
        };

        print!("Current temperature is {}°F and the sky is {}\r\n", current_weather.main.temp, current_weather.weather[0].description);

        Ok(())
    }
}

#[derive(Debug, Deserialize)]
pub struct TomorrowWeather {
    pub list: Vec<TomorrowWeatherList>
}

#[derive(Debug, Deserialize)]
pub struct TomorrowWeatherList {
    pub main: TomorrowWeatherListMain,
    pub dt_txt: String,
    pub weather: Vec<WeatherWeather>
}

#[derive(Debug, Deserialize)]
pub struct TomorrowWeatherListMain {
    pub feels_like: f64,
    pub temp: f64
}

impl TomorrowWeather {
    pub async fn print() -> Result<(), Box<dyn std::error::Error>>{
        let api_value: WeatherSetting = WeatherSetting::read();
        let request_url = format!(
            "https://api.openweathermap.org/data/2.5/forecast?zip={zipcode},us&appid={api_key}&units=imperial",
            zipcode = api_value.zipcode,
            api_key = api_value.open_weather_map_api_key
        );

        let response = reqwest::get(&request_url).await?;

        let tomorrow_weather: Self = match response.status() {
            StatusCode::OK => serde_json::from_str(&response.text().await?)?,
            status => panic!("Request Failed With Code {:?}", status),
        };

        let mut tomorrow_weather_temperature_array = Vec::new();
        let mut tomorrow_sky_status_array = Vec::new();

        for i in 0..8 {
            tomorrow_weather_temperature_array.push(tomorrow_weather.list[i].main.temp);
            tomorrow_sky_status_array.push(tomorrow_weather.list[i].weather[0].description.clone());
        }
        
        tomorrow_sky_status_array.sort();
        tomorrow_sky_status_array.dedup();

        println!("Tomorrow possible sky status: {:#?}", tomorrow_sky_status_array);
        let tomorrow_min_temp = tomorrow_weather_temperature_array.iter().fold(f64::NAN, |a, &b| a.min(b));
        let tomorrow_max_temp = tomorrow_weather_temperature_array.iter().fold(f64::NAN, |a, &b| a.max(b));


        println!("Tomorrow weather is from {}°F to {}°F ", tomorrow_min_temp, tomorrow_max_temp);

        Ok(())
    }
}
