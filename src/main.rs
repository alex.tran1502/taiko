mod cli;
mod weather;
mod todo;

use cli::TaikoRunCommand;
use weather::{CurrentWeather, TomorrowWeather, WeatherSetting};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let app_cmd: TaikoRunCommand = cli::get_command()?;

    if app_cmd.weather_app.get_weather_now {
        CurrentWeather::print().await?;
    }

    if app_cmd.weather_app.get_weather_tomorrow {
        TomorrowWeather::print().await?;
    }

    if app_cmd.weather_app.set_weather_apikey {
        match WeatherSetting::set(&app_cmd.weather_app.weather_apikey.unwrap(), app_cmd.weather_app.zipcode) {
            Ok(result) => println!("{}", result),
            Err(error) => println!("{:?}", error),
        }
    }

    
    Ok(())
}
