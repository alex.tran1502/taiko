use chrono::{DateTime, Utc};

#[derive(Debug)]
pub enum TaskStatus {
    Todo,
    Complete,
}

#[derive(Debug)]
pub struct TaskItem {
    title: String,
    content: String,
    created_date: DateTime<Utc>,
    due_date: DateTime<Utc>,
    status: TaskStatus,
}

impl TaskItem {
    pub fn add(self: &Self) {
        println!("Add Task: {:#?}", self);
    }

    pub fn complete(self: &Self) {
        println!("Complete Task: {:#?}", self);
        Self::remove_task();
    }

    pub fn list_all_tasks() {
        println!("List All Tasks");
    }

    pub fn list_uncomplete_tasks() {
        println!("List Un complete Tasks");
    }

    fn remove_task() {
        println!("Remove task");
    }
}
