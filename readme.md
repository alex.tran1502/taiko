


# Build
## Windows

1. `rustup target add x86_64-pc-windows-gnu`
2. Navigate to `~/.cargo/config`
3. Add
```
    [target.x86_64-pc-windows-gnu]
    linker = "/usr/bin/x86_64-w64-mingw32-gcc"
```

4. Run `make build_windows`